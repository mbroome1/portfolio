/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
          {
            protocol: 'https',
            hostname: 'www.mbroome.nz',
            port: '',
            pathname: '/static/mywebsite/images/**',
          },
        ],
      },
    output: 'standalone',
}

module.exports = nextConfig
