import BackendPill from "@/components/BackendPill";
import FrontendPill from "@/components/FrontendPill";
import GradientLine from "@/components/GradientLine";
import Section from "@/components/Section";

function Projects() {
    return (
        <main className="text-sm md:text-base h-full">
            {/* <div className="bg-[url('/images/hero.jpg')] bg-cover bg-center">
                <div className="bg-gradient-to-r from-slate-900/80 to to-transparent">
                    <div className="max-w-lg md:max-w-xl lg:max-w-4xl xl:max-w-6xl mx-auto ">
                        <h1 className="text-2xl md:text-3xl text-gray-100 p-4 py-6 md:py-8 shadow">
                            Projects
                        </h1>
                    </div>
                </div>
            </div> */}

            <div className="bg-white shadow-sm">
                <h1 className="text-2xl md:text-4xl text-slate-800 font-bold text-center p-4 py-6 md:py-8">
                    Projects
                </h1>
            </div>
            <GradientLine />

            <Section className="flex-1">
                <div className="max-w-lg md:max-w-xl lg:max-w-4xl xl:max-w-6xl mx-auto">
                    <div className="p-4 pt-6 md:pt-12">
                        <h2 className="mt-3 text-xl text-gray-700 ">
                            Experimental applications I have completed in my own
                            time.
                        </h2>

                        <div>
                            <h2 className="my-12 text-xl lg:text-2xl custom-gradient-text font-bold">
                                React Recipes
                            </h2>
                            <div className="flex flex-col lg:flex-row gap-4 md:gap-12 items-center">
                                <div className="basis-1/2">
                                    <img
                                        src="https://www.mbroome.nz/static/mywebsite/images/reactrecipes.jpg"
                                        alt=""
                                        className="shadow rounded p-1"
                                    />
                                    <div className="div"></div>
                                </div>
                                <div className="basis-1/2">
                                    <div className="flex flex-row gap-4 flex-wrap justify-center mt-6 lg:mt-0">
                                        <FrontendPill title="React" />
                                        <FrontendPill title="JavaScript" />
                                        <FrontendPill title="JSON" />
                                        <FrontendPill title="Bootstrap" />
                                        <FrontendPill title="CSS" />
                                    </div>

                                    <div className="flex flex-row gap-4 flex-wrap justify-center mt-6">
                                        <BackendPill title=".NET Core 6 Web API" />
                                        <BackendPill title="C#" />
                                        <BackendPill title="Docker" />
                                    </div>

                                    <div className="mt-8 md:mt-12 flex gap-4 justify-center">
                                        <a
                                            href="https://www.mbroome.nz/reactrecipes/"
                                            target="_blank"
                                            rel="noreferrer"
                                            className="text-lg underline text-blue-900 hover:text-gray-700"
                                        >
                                            View Demo
                                        </a>
                                        <a
                                            href="https://github.com/mbroome1/ReactRecipes/"
                                            target="_blank"
                                            rel="noreferrer"
                                            className="text-lg underline text-gray-500 hover:text-gray-700"
                                        >
                                            View Code
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="border-t-2 mt-14 lg:mt-24"></div>

                        <div>
                            <h2 className="my-12 text-xl lg:text-2xl custom-gradient-text font-bold">
                                Draft Client Template using Next.js
                            </h2>
                            <div className="flex flex-col lg:flex-row gap-4 md:gap-12 items-center">
                                <div className="basis-1/2 lg:order-2">
                                    <img
                                        src="https://www.mbroome.nz/static/mywebsite/images/next_kiwibank.jpg"
                                        alt=""
                                        className="shadow rounded p-1"
                                    />
                                    <div className="div"></div>
                                </div>
                                <div className="basis-1/2 lg:order-1">
                                    <div className="flex flex-row gap-4 flex-wrap justify-center mt-6 lg:mt-0">
                                        <FrontendPill title="Next.js" />
                                        <FrontendPill title="React" />
                                        <FrontendPill title="JavaScript" />
                                        <FrontendPill title="Tailwind CSS" />
                                    </div>

                                    <div className="flex flex-row gap-4 flex-wrap justify-center mt-6">
                                        <BackendPill title="Docker" />
                                    </div>

                                    <div className="mt-8 md:mt-12 flex flex-col gap-4 justify-center items-center">
                                        <a
                                            href="https://www.mbroome.nz/kiwibank/"
                                            target="_blank"
                                            rel="noreferrer"
                                            className="text-lg underline text-blue-900 hover:text-gray-700"
                                        >
                                            View Demo
                                        </a>
                                        <p className="text-gray-500 italic text-sm">
                                            Note: {"'"}Home{"'"}, {"'"}Catalogue
                                            {"'"} and {"'"}Shop Now{"'"} links
                                            working only
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="p-2 md:p-4 mt-12"></div> {/* END */}
            </Section>
        </main>
    );
}

export default Projects;
