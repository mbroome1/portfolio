import GradientLine from "@/components/GradientLine";
import Section from "@/components/Section";
import WorkCardA from "@/components/WorkCardA";
import WorkCardB from "@/components/WorkCardB";

export default function Work() {
    return (
        <main className="text-sm md:text-base">
            <div className="bg-white shadow-sm">
                <h1 className="text-2xl md:text-4xl text-slate-800 font-bold text-center p-4 py-6 md:py-8">
                    Work Experience
                </h1>
            </div>
            <GradientLine />

            <Section className="max-w-lg md:max-w-xl lg:max-w-4xl xl:max-w-6xl 2xl:max-w-7xl mx-auto">
                <div className="mt-4 md:mt-6 p-4">
                    <WorkCardA
                        title="The Uniform Centre"
                        subtitle=""
                        link="https://www.uniform-centre.co.nz"
                        description="Creating a fresh new look using modern HTML, CSS and JavaScript to compliment a major backend overhaul."
                        image="https://www.mbroome.nz/static/mywebsite/images/uniform/uniform-centre_home2.jpg"
                        reverseOrder={false}
                    />

                    <WorkCardA
                        title="Meeting Change"
                        subtitle="Book Ordering Website"
                        link="https://www.meetingchange.nz"
                        description="On behalf of New Zealand Meatboard through their agent, developed a single page website for ordering a newly released book celebrating a 100 years milestone within the meat industry."
                        image="https://www.mbroome.nz/static/mywebsite/images/meeting/meeting-change_home2.jpg"
                        reverseOrder={true}
                    />
                </div>
            </Section>

            {/* Booker Spalding */}
            <Section className="max-w-lg md:max-w-xl lg:max-w-4xl xl:max-w-6xl 2xl:max-w-7xl mx-auto">
                <div className="mt-12">
                    <div className="p-4 pt-6 md:pt-12">
                        <h2 className="text-xl lg:text-2xl custom-gradient-text font-bold">
                            Booker ~ Spalding
                        </h2>
                        <p className="mt-6">
                            Creating, maintaining and modernising client
                            ordering websites using PHP and SQL for the backend,
                            HTML, CSS and JavaScript for the frontend.{" "}
                        </p>
                        <p className="text-gray-900 font-semibold italic mt-4">
                            NOTE: &nbsp; Sensitive information has been crossed
                            out.
                        </p>

                        <div className="lg:flex gap-4">
                            <WorkCardB
                                title={"Home Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/kiwibank_home.jpg"
                                }
                            />
                            <WorkCardB
                                title={"Catalogue Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/kiwibank_catalogue.jpg"
                                }
                            />
                        </div>
                        <div className="lg:flex gap-4">
                            <WorkCardB
                                title={"Item Detail Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/kiwibank_detail.jpg"
                                }
                            />
                            <WorkCardB
                                title={"Cart Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/kiwibank_cart.jpg"
                                }
                            />
                        </div>
                        <div className="lg:flex gap-4">
                            <WorkCardB
                                title={"Home Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/bs_heritage_1.jpg"
                                }
                            />
                            <WorkCardB
                                title={"Catalogue Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/bs_heritage_2.jpg"
                                }
                            />
                        </div>
                        <div className="lg:flex gap-4">
                            <WorkCardB
                                title={"Cart Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/bs_heritage_3.jpg"
                                }
                            />
                            <WorkCardB
                                title={"Home Page"}
                                image={
                                    "https://www.mbroome.nz/static/mywebsite/images/bookers/bs_ravensdown_1.jpg"
                                }
                            />
                        </div>
                    </div>
                </div>{" "}
                {/* div before section */}
                <div className="p-2 md:p-4 mt-12"></div> {/* END */}
            </Section>
        </main>
    );
}
