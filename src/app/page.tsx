import HomePage from "@/components/Home";
import Hero from "@/components/Hero";
import Section from "@/components/Section";
import TechCard from "@/components/TechCard";
import SolidLine from "@/components/SolidLine";
import LinkedInHomeLink from "@/components/LinkedInHomeLink";

export default function Home() {
    return (
        <main className="flex flex-col text-sm md:text-base">
            <Hero />
            <Section>
                <div className="mt-8">
                    <SolidLine />
                </div>
                <div className="mt-4 p-4">
                    <HomePage />
                </div>
                <div className="mt-8">
                    <SolidLine />
                </div>

                {/* Contact Me */}
                <div className="p-4 mt-6 xl:mt-12 flex flex-col items-center text-left text-sm lg:text-lg py-12">
                    <p>
                        Open for a friendly discussion about new opportunities!
                        You can contact me via LinkedIn.
                    </p>
                    <div className="flex mt-8 text-xl">
                        <LinkedInHomeLink />
                    </div>
                </div>

                {/* Technologies of Interest */}
                <div className="mt-4 md:mt-6 lg:mt-12 p-4 text-center md:text-center">
                    <h1 className="text-lg md:text-xl lg:text-2xl font-semibold tracking-wider mb-12">
                        Technologies of Interest
                    </h1>

                    <div className="flex justify-around flex-col lg:flex-row lg:gap-4 xl:gap-12 xl:px-8">
                        <div className="basis-full lg:basis-1/2">
                            <h2 className="text-lg text-blue-700 my-8 tracking-wider">
                                Frontend
                            </h2>
                            <div className="p-3 flex flex-wrap gap-4 gap-y-6 md:gap-4 lg:gap-6 justify-center md:justify-center">
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/html-5.svg"
                                    alt="HTML 5"
                                    title="HTML 5"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/css-3.svg"
                                    alt="CSS 3"
                                    title="CSS 3"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/javascript.svg"
                                    alt="JavaScript"
                                    title="JavaScript"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/tailwindcss-icon.svg"
                                    alt="Tailwind CSS"
                                    title="Tailwind CSS"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/react.svg"
                                    alt="React"
                                    title="React"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/nextjs.svg"
                                    alt="Next.js"
                                    title="Next.js"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/redux.svg"
                                    alt="Redux"
                                    title="Redux"
                                />
                            </div>
                        </div>

                        <div className="hidden lg:block w-0.5 h-128 bg-slate-200"></div>

                        <div className="basis-full lg:basis-1/2">
                            <h2 className="text-lg text-blue-700 my-8 tracking-wider">
                                Backend
                            </h2>
                            <div className="p-2 flex flex-wrap gap-4 gap-y-6 md:gap-4 lg:gap-6 justify-center md:justify-center">
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/sql-database-generic.svg"
                                    alt="SQL"
                                    title="SQL"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/api.svg"
                                    alt="REST API"
                                    title="REST API"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/nodejs-icon.svg"
                                    alt="Node.js"
                                    title="Node.js"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/postgresql.svg"
                                    alt="PostgreSQL"
                                    title="PostgreSQL"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/c-sharp.svg"
                                    alt="C#"
                                    title="C#"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/dotnet-core.svg"
                                    alt=".NET Core"
                                    title=".NET Core"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/microsoft-azure.svg"
                                    alt="Azure"
                                    title="Azure"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/php.svg"
                                    alt="PHP"
                                    title="PHP"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/nginx.svg"
                                    alt="Nginx"
                                    title="Nginx"
                                />
                                <TechCard
                                    src="https://www.mbroome.nz/static/mywebsite/images/svg/python.svg"
                                    alt="Python"
                                    title="Python"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <h2 className="text-lg text-blue-700 my-8 tracking-wider">
                            Tools
                        </h2>
                        <div className="p-2 flex flex-wrap mb-6 gap-4 gap-y-6 md:gap-4 lg:gap-6 justify-center md:justify-center">
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/visual-studio-code.svg"
                                alt="VS Code"
                                title="Visual Studio Code"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/visual-studio.svg"
                                alt="Visual Studio"
                                title="Visual Studio"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/ubuntu.svg"
                                alt="Ubuntu Linux"
                                title="Ubuntu Linux"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/docker-icon.svg"
                                alt="Docker"
                                title="Docker"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/git-icon.svg"
                                alt="Git"
                                title="Git"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/github-icon.svg"
                                alt="Github"
                                title="Github"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/gitlab.svg"
                                alt="Gitlab"
                                title="Gitlab CI CD"
                            />
                            <TechCard
                                src="https://www.mbroome.nz/static/mywebsite/images/svg/power-bi.svg"
                                alt="Power BI"
                                title="Power BI"
                            />
                        </div>
                    </div>
                </div>
            </Section>
        </main>
    );
}
