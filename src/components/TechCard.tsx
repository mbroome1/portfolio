import Image from "next/image";

type PropTypes = {
    src: string;
    alt: string;
    title: string;
};

export default function TechCard({ src, alt, title }: PropTypes) {
    return (
        <div className="flex flex-col items-center whitespace-wrap">
            <div className="relative w-8 h-8 lg:w-10 lg:h-10 ">
                <Image
                    src={src}
                    alt={alt}
                    style={{ objectFit: "contain" }}
                    fill={true}
                    loading="lazy"
                />
            </div>
            <h3 className="text-xs text-gray-600 mt-3 lg:mt-4 w-12 xl:w-14 text-center">
                {title}
            </h3>
        </div>
    );
}
