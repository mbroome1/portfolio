import Link from "next/link";
import React from "react";
import { BsGithub } from "react-icons/bs";

const GitHubLink = () => {
    return (
        <Link
            href={"https://github.com/mbroome1"}
            target="_blank"
            title="GitHub"
            className="text-slate-800 hover:text-gray-500 flex flex-col items-center"
        >
            <BsGithub />
            <span className="text-sm text-slate-500 pt-2.5">GitHub</span>
        </Link>
    );
};

export default GitHubLink;
