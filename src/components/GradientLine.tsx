const GradientLine = () => {
    return (
        <div className="bg-gradient-to-r from-slate-200 to-slate-400 h-1"></div>
    );
};

export default GradientLine;
