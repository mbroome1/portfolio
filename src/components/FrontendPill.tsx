type Prop = {
    title: string;
};

const FrontendPill = ({ title }: Prop) => {
    return <div className="bg-blue-50 text-sm p-2 shadow rounded">{title}</div>;
};

export default FrontendPill;
