import { BsLinkedin } from "react-icons/bs";
import LinkedInLink from "./LinkedInLink";
import Link from "next/link";
import Image from "next/image";
import LinkedInHomeLink from "./LinkedInHomeLink";

const Home = () => {
    return (
        <>
            <div className="mt-4">
                <div className="py-4 flex flex-col lg:flex-row items-center justify-center gap-12 md:gap-4 lg:gap-6">
                    <div className="order-2 lg:order-2 basis-1/2 xl:basis-2/3 text-center lg:text-left text-xl md:text-2xl font-light leading-relaxed md:leading-relaxed lg:leading-relaxed ">
                        <p>
                            {" "}
                            <span className="font-semibold">
                                Full Stack Web Developer
                            </span>{" "}
                            with experience in{" "}
                            <span className="underline decoration-2 underline-offset-3 decoration-blue-300">
                                creating
                            </span>
                            {", "}
                            <span className="underline decoration-2 underline-offset-3 decoration-blue-300">
                                modernising
                            </span>
                            {", "}
                            <span className="underline decoration-2 underline-offset-3 decoration-blue-300">
                                modifying
                            </span>{" "}
                            and{" "}
                            <span className="underline decoration-2 underline-offset-3 decoration-blue-300">
                                maintaining
                            </span>{" "}
                            websites.
                        </p>
                    </div>
                    <div className="basis-1/2 xl:basis-1/3 w-56 lg:w-full order-1 lg:order-1">
                        <Image
                            src={"/images/web.svg"}
                            alt=""
                            width={1357}
                            height={887}
                            style={{ objectFit: "cover" }}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
//
export default Home;
