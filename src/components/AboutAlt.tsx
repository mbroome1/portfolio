import Image from "next/image";
// bg-slate-100 rounded-lg shadow
const About = () => {
    return (
        <>
            <div className="mt-4 flex gap-3 lg:gap-6 justify-center md:justify-normal ">
                <div className="grow py-4 px-6 flex flex-col md:flex-row items-center gap-6 md:gap-4 lg:gap-6 ">
                    {/* <div className="shrink-0 relative h-28 w-28 md:h-24 md:w-24">
                        <Image
                            src="/images/brackets.png"
                            alt=""
                            fill={true}
                            style={{ objectFit: "contain" }}
                        />
                    </div> */}
                    <p className="md:pl-4 text-2xl md:text-3xl lg:text-4xl font-light leading-relaxed lg:leading-relaxed text-center md:text-left">
                        A{" "}
                        <span className="font-semibold">
                            Full Stack Web Developer
                        </span>{" "}
                        with experience in{" "}
                        <span className="underline decoration-2 underline-offset-3 decoration-blue-200">
                            creating
                        </span>
                        {", "}
                        <span className="underline decoration-2 underline-offset-3 decoration-blue-200">
                            modernising
                        </span>
                        {", "}
                        <span className="underline decoration-2 underline-offset-3 decoration-blue-200">
                            modifying
                        </span>{" "}
                        and{" "}
                        <span className="underline decoration-2 underline-offset-3 decoration-blue-200">
                            maintaining
                        </span>{" "}
                        websites.
                    </p>
                </div>
            </div>
            <div className="mt-4 flex gap-3 lg:gap-6 justify-center md:justify-left border">
                <div className="grow py-6 lg:py-12 px-6 flex flex-col md:flex-col items-center gap-6 md:gap-4 lg:gap-6 bg-blue-50">
                    <div className="shrink-0 relative h-28 w-28 md:h-24 md:w-24">
                        <Image
                            src="/images/graduate.png"
                            alt=""
                            fill={true}
                            style={{ objectFit: "cover" }}
                        />
                    </div>
                    <div className="md:pl-4 text-sm text-center md:text-left lg:text-xl">
                        <h3 className="font-bold">
                            Bachelor{"'"}s Degree in Commerce and Administration
                        </h3>
                        <p className="pt-1">
                            <span className="font-bold custom-gradient-text">
                                Information Systems
                            </span>{" "}
                            and{" "}
                            <span className="font-bold custom-gradient-text">
                                E-Commerce
                            </span>
                        </p>

                        <div className="font-bold text-slate-500 pt-1">
                            Victoria University of Wellington
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default About;
