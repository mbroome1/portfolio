import Link from "next/link";
import React from "react";
import { BsLinkedin } from "react-icons/bs";

const LinkedInHomeLink = () => {
    return (
        <Link
            href={"https://www.linkedin.com/in/michael-broome-3373b9a7/"}
            target="_blank"
            title="LinkedIn"
            className="gap-2 bg-blue-600 hover:bg-blue-700 text-white flex flex-row items-center shadow-lg rounded-lg px-3 py-2"
        >
            <BsLinkedin />
            <span className="text-sm">LinkedIn</span>
        </Link>
    );
};

export default LinkedInHomeLink;
