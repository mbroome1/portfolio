const About = () => {
    return (
        <>
            <p className="py-2">
                Currently a{" "}
                <span className="font-bold">Full Stack Web Developer</span> with
                work experience in creating, modifying and maintaining websites.
            </p>
            <p className="py-2">
                I have a{" "}
                <span className="font-bold">
                    Bachelor{"'"}s Degree in Commerce and Administration
                </span>{" "}
                with majors in{" "}
                <span className="font-bold">Information Systems</span> and{" "}
                <span className="font-bold">E-Commerce</span> from{" "}
                <span className="font-bold">
                    Victoria University of Wellington
                </span>
                .
            </p>
            <p className="py-2">
                I am always striving to learn new skills and becoming a better
                developer. Skills acquired include: creating{" "}
                <span className="font-bold">REST APIs</span> with{" "}
                <span className="font-bold">.NET Core</span> using{" "}
                <span className="font-bold">C#</span>, creating APIs with{" "}
                <span className="font-bold">Node.js</span>, creating UI{"'"}s
                with <span className="font-bold">React</span>,{" "}
                <span className="font-bold">Next.js</span> and modern{" "}
                <span className="font-bold">JavaScript</span>, and setting up a
                cloud environment in{" "}
                <span className="font-bold">Microsoft Azure</span> for my
                finished code.
            </p>
            <p className="py-2">
                Currently I am gaining skills in{" "}
                <span className="font-bold">Data Analytics</span> through taking
                online courses on <span className="font-bold">Data Camp</span>{" "}
                with focus on advanced querying in{" "}
                <span className="font-bold">SQL</span>, and data visualisation
                using <span className="font-bold">Power BI</span>.
            </p>
        </>
    );
};

export default About;
