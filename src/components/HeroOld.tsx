const Hero = () => {
    return (
        <section className="bg-[url('/images/hero.jpg')] bg-cover bg-bottom lg:bg-center">
            <div className="bg-black/20">
                <div className="max-w-2xl mx-auto min-h-[40vh] xl:min-h-[50vh] 2xl:min-h-[60vh] flex flex-col justify-center items-center text-center ">
                    <div className="p-2 md:p-8 backdrop-blur-sm bg-white/5 rounded-lg">
                        <h1 className="text-3xl lg:text-5xl py-5 pt-1 text-white font-semibold tracking-wider">
                            Hello, I{"'"}m Michael
                        </h1>
                        <h2 className="text-lg lg:text-2xl text-sky-300 font-semibold">
                            Web Developer
                        </h2>
                        <h3 className="text-base lg:text-xl text-gray-400 pt-2 font-bold">
                            Wellington, New Zealand
                        </h3>
                        <p className="text-base lg:text-xl pt-5 text-sky-200 font-bold tracking-wider">
                            Welcome to my newly created website!
                        </p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
