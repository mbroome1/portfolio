type Prop = {
    title: string;
};

const BackendPill = ({ title }: Prop) => {
    return (
        <div className="bg-blue-100 text-sm p-2 shadow rounded">{title}</div>
    );
};

export default BackendPill;
