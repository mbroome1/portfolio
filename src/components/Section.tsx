import { ReactNode } from "react";

type Prop = {
    children: ReactNode;
    className?: string;
};

const Section = ({ children }: Prop) => {
    return (
        <section className="w-full max-w-lg md:max-w-xl lg:max-w-2xl xl:max-w-4xl 2xl:max-w-6xl mx-auto">
            {children}
        </section>
    );
};

export default Section;
