type Prop = {
    title: string;
    subtitle?: string;
    link: string;
    description: string;
    image: string;
    reverseOrder: boolean;
};

const WorkCardA = ({
    title,
    subtitle,
    link,
    description,
    image,
    reverseOrder,
}: Prop) => {
    return (
        <div className="flex flex-col lg:flex-row gap-6 mt-12 items-center">
            <div
                className={`mt-6 md:basis-5/12 ${
                    reverseOrder && "lg:order-2 lg:ml-6"
                }`}
            >
                <h2 className="text-xl lg:text-2xl custom-gradient-text font-bold">
                    {title}
                </h2>
                {subtitle && (
                    <h5 className="text-sm mt-1 text-gray-600">{subtitle}</h5>
                )}
                <div className="mt-3 lg:mt-6">
                    <a
                        href={link}
                        target="_blank"
                        rel="noreferrer"
                        className="text-blue-900 underline hover:text-gray-700"
                    >
                        View Website
                    </a>
                </div>
                <p className="mt-4 lg:mt-6">{description}</p>
            </div>
            <div
                className={`mt-6 md:basis-7/12  ${
                    reverseOrder && "lg:order-1"
                }`}
            >
                <div className="shadow rounded">
                    <img src={image} alt="" className="rounded" />
                </div>
            </div>
        </div>
    );
};

export default WorkCardA;
