import Link from "next/link";
import { usePathname } from "next/navigation";
import { ReactNode } from "react";

type Prop = {
    icon: ReactNode;
    text: string;
    route: string;
    isActive: boolean;
    handleMenuClose: () => void;
};

const SidebarListItem = ({
    icon,
    text,
    route,
    isActive,
    handleMenuClose,
}: Prop) => {
    const pathname = usePathname();

    return (
        <li className="">
            {/* <Link
                href={route}
                className={`text-gray-100 font-semibold px-3 py-3 rounded flex items-center text-lg hover:bg-slate-800 ${
                    isActive && "bg-slate-800"
                }`}
                onClick={handleMenuClose}
            > */}
            <Link
                href={route}
                className={`text-slate-700 font-semibold px-3 py-3 rounded flex items-center text-lg hover:bg-slate-100 ${
                    isActive && "bg-slate-100 shadow-sm"
                }`}
                onClick={handleMenuClose}
            >
                <span className="pr-6">{icon}</span>
                {text}
            </Link>
        </li>
    );
};

export default SidebarListItem;
