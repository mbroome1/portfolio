"use client";

import { RiMenu5Fill } from "react-icons/ri";
import { useSidebarContext } from "@/context/SidebarContext";

export default function Content({ children }: React.PropsWithChildren) {
    const { isOpen, setIsOpen } = useSidebarContext();

    const toggleMenuButton = () => {
        setIsOpen(!isOpen);
    };

    const handleMenuClose = () => {
        isOpen && setIsOpen(false);
    };

    return (
        <div className="flex-1 flex flex-col ml-0 md:ml-72 overflow-y-auto">
            <div className="absolute top-4 right-6 z-40">
                <button
                    className="text-xl text-white bg-blue-800 rounded-full w-10 h-10 flex justify-center items-center shadow-black shadow-2xl md:hidden"
                    onClick={toggleMenuButton}
                >
                    <RiMenu5Fill />
                </button>
            </div>
            <div className="flex-grow">{children}</div>
            <div
                className={`${
                    isOpen ? "bg-black/50 z-30 h-screen w-screen fixed" : ""
                }`}
                onClick={handleMenuClose}
            ></div>
        </div>
    );
}
