import Image from "next/image";

type Props = {
    title: string;
    image: string;
};

const WorkCardB = ({ title, image }: Props) => {
    return (
        <div className="h-full flex flex-col lg:flex-col gap-6 mt-12 items-center border-t">
            <div className="mt-6">
                <h3 className="text-lg xl:text-xl mt-1 text-slate-900 font-semibold">
                    {title}
                </h3>
            </div>
            <div className="mt-2 md:mt-6 md:mb-6">
                <div className="shadow relative">
                    <Image
                        src={image}
                        alt=""
                        className="rounded object-cover"
                        width={1916}
                        height={960}
                        style={{ objectFit: "cover" }}
                    />
                </div>
            </div>
        </div>
    );
};

export default WorkCardB;
