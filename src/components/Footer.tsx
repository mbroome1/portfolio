export default function Footer() {
    return (
        <div className="text-gray-400 p-2 text-sm text-center">
            <p>Created by Michael Broome</p>
            <div className="my-1 text-center text-xs underline">
                <a href="https://storyset.com/online">
                    Home illustration from Storyset
                </a>
            </div>
            <p>&copy; {new Date().getFullYear()}</p>
        </div>
    );
}
