import Section from "./Section";

const HeroTwo = () => {
    return (
        <Section>
            <div className="text-center">
                <div className="py-12 lg:py-16">
                    <div className="">
                        <h1
                            className={`text-3xl md:text-3xl lg:text-4xl xl:text-5xl pt-1 text-slate-700 font-bold tracking-wide`}
                        >
                            Hello! I{"'"}m Michael
                        </h1>
                        <h2 className="text-lg md:text-2xl 2xl:text-3xl pt-3 lg:pt-6 text-slate-600 font-semibold">
                            <span className="custom-gradient-text">
                                Web Developer
                            </span>{" "}
                            from{" "}
                            <span className="custom-gradient-text">
                                Wellington, New Zealand
                            </span>
                        </h2>
                        <p className="text-base lg:text-2xl pt-3 lg:pt-6 text-slate-500 font-semibold tracking-wider">
                            Welcome to my website!
                        </p>
                    </div>
                </div>
            </div>
        </Section>
    );
};

export default HeroTwo;
