import Link from "next/link";
import React from "react";
import { BsLinkedin } from "react-icons/bs";

const LinkedInLink = () => {
    return (
        <Link
            href={"https://www.linkedin.com/in/michael-broome-3373b9a7/"}
            target="_blank"
            title="LinkedIn"
            className="text-blue-600 hover:text-blue-400 flex flex-col items-center"
        >
            <BsLinkedin />
            <span className="text-xs text-slate-400 pt-2.5 tracking-wide">
                LinkedIn
            </span>
        </Link>
    );
};

export default LinkedInLink;
