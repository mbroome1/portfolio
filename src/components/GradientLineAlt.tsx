const GradientLineAlt = () => {
    return (
        // <div className="bg-gradient-to-r from-blue-200 to-blue-800 h-1"></div>
        <div className="bg-gradient-to-r from-slate-400 to-slate-200 h-1"></div>
    );
};

export default GradientLineAlt;
