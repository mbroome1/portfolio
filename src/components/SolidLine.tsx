import React from "react";

const SolidLine = () => {
    return <div className="bg-gray-200 h-1"></div>;
};

export default SolidLine;
