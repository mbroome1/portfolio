"use client";
import { usePathname } from "next/navigation";
import { FaTimes } from "react-icons/fa";
import { BsBriefcase, BsClipboardData, BsHouse } from "react-icons/bs";
import { useSidebarContext } from "@/context/SidebarContext";
import Image from "next/image";
import Footer from "./Footer";
import SidebarListItem from "./SidebarListItem";
import LinkedInLink from "./LinkedInLink";
import GitHubLink from "./GitHubLink";
import GitLabLink from "./GitLabLink";

export default function Sidebar() {
    const pathname = usePathname();

    const { isOpen, setIsOpen } = useSidebarContext();

    const handleMenuClose = () => {
        setIsOpen(false);
    };

    const menuLinks = [
        {
            icon: <BsHouse />,
            text: "Home",
            route: "/",
        },
        {
            icon: <BsBriefcase />,
            text: "Work Experience",
            route: "/work",
        },
        {
            icon: <BsClipboardData />,
            text: "Projects",
            route: "/projects",
        },
    ];

    return (
        <div
            className={`fixed w-72 h-screen overflow-y-auto flex flex-col bg-white shadow-xl shadow-slate-300 z-50 md:ml-0 tranform-all duration-100 ${
                isOpen ? "ml-0" : "-ml-72"
            }`}
        >
            <div className="p-2 text-right">
                <button
                    className="text-3xl text-gray-700 hover:text-gray-500 md:hidden ml-auto"
                    onClick={handleMenuClose}
                >
                    <FaTimes />
                </button>
            </div>
            <h1 className="text-2xl text-slate-600 font-semibold text-center py-2 mb-4 md:mb-8">
                Michael Broome
            </h1>
            <div className="text-center">
                <div className="inline-block text-center mx-auto overflow-hidden rounded-full shadow-2xl">
                    <div className="relative h-32 w-32 rounded-full">
                        <Image
                            src="https://www.mbroome.nz/static/mywebsite/images/profile.jpg"
                            alt=""
                            fill={true}
                            style={{ objectFit: "cover" }}
                        />
                    </div>
                </div>
            </div>

            <div className="mx-6 mt-6 text-center">
                <h2 className="text-lg text-slate-700 font-semibold py-1">
                    Web Developer
                </h2>
            </div>
            <div className="flex justify-center gap-6 mx-6 mt-6 lg:mt-6 text-2xl text-center">
                <LinkedInLink />
                {/* <GitHubLink />
                <GitLabLink /> */}
            </div>

            <hr className="h-px bg-gray-300 border-0 m-4 mt-8" />

            {/* Sidebar Menu Links */}
            <div className="flex flex-col flex-grow">
                <ul className="flex-grow mx-4 my-6 flex flex-col gap-2 mb-auto tracking-wider">
                    {menuLinks.map((link) => (
                        <SidebarListItem
                            key={link.text}
                            icon={link.icon}
                            text={link.text}
                            route={link.route}
                            isActive={link.route === pathname ? true : false}
                            handleMenuClose={handleMenuClose}
                        />
                    ))}
                </ul>
            </div>

            <Footer />
        </div>
    );
}
