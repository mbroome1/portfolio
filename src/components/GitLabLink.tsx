import Link from "next/link";
import React from "react";
import { PiGitlabLogoFill } from "react-icons/pi";

const GitLabLink = () => {
    return (
        <Link
            href={"https://gitlab.com/mbroome1"}
            target="_blank"
            title="GitLab"
            className="text-orange-500 hover:text-orange-300 flex flex-col items-center"
        >
            <PiGitlabLogoFill />
            <span className="text-sm text-slate-500 pt-2.5">GitLab</span>
        </Link>
    );
};

export default GitLabLink;
