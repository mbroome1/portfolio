"use client";

import {
    createContext,
    useContext,
    Dispatch,
    SetStateAction,
    useState,
} from "react";

type ContextType = {
    isOpen: boolean;
    setIsOpen: Dispatch<SetStateAction<boolean>>;
};

type ProviderProps = {
    children: React.ReactNode;
};

const SidebarContext = createContext<ContextType>({
    isOpen: false,
    setIsOpen: () => {},
});

export const SidebarContextProvider = ({ children }: ProviderProps) => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <SidebarContext.Provider value={{ isOpen, setIsOpen }}>
            {children}
        </SidebarContext.Provider>
    );
};

export const useSidebarContext = () => useContext(SidebarContext);
